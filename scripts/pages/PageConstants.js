const PageConstants = { }

PageConstants.PAGE_LOGIN                    = "pgLogin"; 
PageConstants.PAGE_MY_LEAVE_REQUESTS        = "pgMyLeaveRequests"; 
PageConstants.PAGE_MY_LEAVE_REQUEST_DETAIL  = "pgMyLeaveRequestDetail"; 
PageConstants.PAGE_MY_APPROVAL_WORKLIST     = "pgMyApprovalWorklist"; 
PageConstants.PAGE_NEW_LEAVE_REQUEST        = "pgNewLeaveRequest";
PageConstants.PAGE_STATUS                   = "pgStatus";
PageConstants.PAGE_OUT_OF_OFFICE            = "pgOutOfOffice";
PageConstants.PAGE_ABOUT                    = "pgAbout";

module.exports = PageConstants;