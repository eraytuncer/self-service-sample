const extend = require("js-base/core/extend");

const Image         = require('nf-core/ui/image');
const HeaderBarItem = require("nf-core/ui/headerbaritem");
const Router = require("nf-core/ui/router");
const PageConstants = require("pages/PageConstants");



// Get generetad UI code
var PageNewRequestDesign = require("../ui/ui_pgNewRequest");

const PageNewRequest = extend(PageNewRequestDesign)(
    function(_super) {
        var self = this;
        _super(self);
        
        Router.sliderDrawer.enabled = false;

        this.mapChildren(function(component, componentName) {
            self[componentName] = component;
        });
        
        this.rootLayout.mapChildren(function(component, componentName) {
            self.rootLayout[componentName] = component;
        });

        // Set Back Button
        var leftItem = new HeaderBarItem({
            image: Image.createFromFile("images://back.png"),
            onPress: function() {
                Router.goBack();
            }
        });
        this.headerBar.setLeftItem(leftItem);
        
        
        var leaveTypeSelectedIndex;
        var timeUnitSelectedIndex;
        var selectedStartDate;
        var selectedEndDate;
        
        

});

module && (module.exports = PageNewRequest);