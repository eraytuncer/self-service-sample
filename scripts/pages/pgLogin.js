const extend = require("js-base/core/extend");
const Router = require("nf-core/ui/router");
const PageConstants = require("pages/PageConstants");

// Get generetad UI code
var PageLoginDesign = require("../ui/ui_pgLogin");

const PageLoginRequest = extend(PageLoginDesign)(
    function(_super) {
        var self = this;
        _super(self);

        var uiComponents = {};
        this.onShow = onShow.bind(this);
        this.mapChildren(componentTraverse);

        this.mapChildren(function(component, componentName) {
            self[componentName] = component;
        });
        
        this.rootLayout.mapChildren(function(component, componentName) {
            self.rootLayout[componentName] = component;
        });
        
        uiComponents.usernameTextbox.hint = "Username";
        uiComponents.passwordTextbox.hint = "Password";        
        uiComponents.passwordTextbox.isPassword = true;      
        
        uiComponents.loginLabel.onTouch = function()
        {
            Router.go(PageConstants.PAGE_STATUS);
        }

        function componentTraverse(component, componentName) {
            if (component.mapChildren) {
                component.mapChildren(componentTraverse);
            }
            uiComponents[componentName] = component;
        }
        
    }
);

function onShow() {
    Router.sliderDrawer.enabled = false;
}

module && (module.exports = PageLoginRequest);