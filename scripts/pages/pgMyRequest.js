const extend = require("js-base/core/extend");

const Font          = require('nf-core/ui/font');
const Image         = require('nf-core/ui/image');
const HeaderBarItem = require("nf-core/ui/headerbaritem");
const Router        = require("nf-core/ui/router");
const PageConstants = require("pages/PageConstants");



// Get generetad UI code
var PageMyRequestDesign = require("../ui/ui_pgMyRequest");

const PageMyRequest = extend(PageMyRequestDesign)(
    function(_super) {
        var self = this;
        _super(self);
        
        Router.sliderDrawer.enabled = true;

        this.mapChildren(function(component, componentName) {
            self[componentName] = component;
        });
        
        this.rootLayout.mapChildren(function(component, componentName) {
            self.rootLayout[componentName] = component;
        });

        // Set Back Button
        var leftItem = new HeaderBarItem({
            image: Image.createFromFile("images://back.png"),
            onPress: function() {
                Router.goBack();
            }
        });
        this.headerBar.setLeftItem(leftItem);
        
        // Set Remove Button
        this.rootLayout.removeButton.font = Font.create("FontAwesome", 28);
        this.rootLayout.removeButton.text = JSON.parse('"\uf1f8"');

});

module && (module.exports = PageMyRequest);