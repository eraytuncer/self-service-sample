const extend = require("js-base/core/extend");

const Image         = require('nf-core/ui/image');
const HeaderBarItem = require("nf-core/ui/headerbaritem");
const Router        = require("nf-core/ui/router");
const PageConstants = require("pages/PageConstants");



// Get generetad UI code
var PageStatusDesign = require("../ui/ui_pgStatus");

const PageStatus = extend(PageStatusDesign)(
    function(_super) {
        var self = this;
        _super(self);
        
        this.onShow = onShow.bind(this);
        var uiComponents = {};
        this.mapChildren(componentTraverse);

        this.mapChildren(function(component, componentName) {
            self[componentName] = component;
        });
        
        this.rootLayout.mapChildren(function(component, componentName) {
            self.rootLayout[componentName] = component;
        });
        
        // Set Menu Button
        var leftItem = new HeaderBarItem({
            image: Image.createFromFile("images://menu.png"),
            onPress: function() {
            }
        });
        this.headerBar.setLeftItem(leftItem);
        
        uiComponents.plusImage.onTouch = function ()
        {
            Router.go(PageConstants.PAGE_NEW_LEAVE_REQUEST);
        }
        
        uiComponents.usedLayout.onTouch = goToMyRequests;
        uiComponents.totalLayout.onTouch = goToMyRequests;
        uiComponents.remainingLayout.onTouch = goToMyRequests;
        
        function componentTraverse(component, componentName) {
            if (component.mapChildren) {
                component.mapChildren(componentTraverse);
            }
            uiComponents[componentName] = component;
        }
        
        
        function goToMyRequests()
        {
            Router.go(PageConstants.PAGE_MY_LEAVE_REQUESTS);
        }
        
});

function onShow() {
    Router.sliderDrawer.enabled = true;
}

module && (module.exports = PageStatus);