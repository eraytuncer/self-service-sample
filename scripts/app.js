/* globals lang */
require("i18n/i18n.js"); //generates global lang object
require('libs/utils/JSON.prune.js');
require("libs/Oracle/smartface.mcs.js");

const Application       = require("nf-core/application");
const Router            = require("nf-core/ui/router");
const PageConstants     = require("pages/PageConstants");
const FlexLayout        = require('nf-core/ui/flexlayout');
const Color             = require('nf-core/ui/color');
const ImageView         = require('nf-core/ui/imageview');
const Image             = require('nf-core/ui/image');
const Font              = require('nf-core/ui/font');
const Label             = require("nf-core/ui/label");
const TextAlignment     = require('nf-core/ui/textalignment');
const ListView          = require('nf-core/ui/listview');
const ListViewItem      = require("nf-core/ui/listviewitem");
const SliderDrawer      = require('nf-core/ui/sliderdrawer');
const System            = require("nf-core/device/system");


// Set uncaught exception handler, all exceptions that are not caught will
// trigger onUnhandledError callback.
Application.onUnhandledError = function (e) {
    alert({
        title: lang.applicationError,
        message: e.message + "\n\n*" + e.sourceURL + "\n*" + e.line + "\n*" + e.stack
    });
};

// Global objects
var smfOracle;
var backendName = "smartfaceOracleMCS";
var oMenuItems;
var oProfile;
var oTimeTable;
var oLeaveRequestList;
var oTimecardList;
var templateOutOfOfficeText = "Hello,\n\nI'm currently out of the office but I will return on {EndDate}. I will respond to your inquiry as soon as possible.\n\nBest\n{FullName}\n{Role}/{Team}";
var lunchBreakDuration = 1;
var dayWorkHoursStart = 9;
var dayWorkHoursEnd = 18;


// Define routes and go to initial page of application
Router.add(PageConstants.PAGE_LOGIN, require("./pages/pgLogin"));
Router.add(PageConstants.PAGE_MY_LEAVE_REQUESTS, require("./pages/pgMyRequest"));
Router.add(PageConstants.PAGE_NEW_LEAVE_REQUEST, require("./pages/pgNewRequest"));
Router.add(PageConstants.PAGE_STATUS, require("./pages/pgStatus"));


// Creating a new Oracle MCS instance 
//smfOracle = new SMF.Oracle.MobileCloudService(backendName);
// logging in as anonymous user to log Analytics events
// if you need you can auth user with .authenticate
//smfOracle.authAnonymous();
// logging an event
//smfOracle.logAnalytics('Application_OnStart');


Router.go(PageConstants.PAGE_LOGIN);
addSliderDrawer()


function addSliderDrawer()
{
    var sliderDrawer = new SliderDrawer();
    sliderDrawer.width = 250;
    if(System.language.indexOf("ar") != -1)
    {
        sliderDrawer.drawerPosition = SliderDrawer.Position.RIGHT;
    }else
    {
        sliderDrawer.drawerPosition = SliderDrawer.Position.LEFT;

    }
    
    
    sliderDrawer.onLoad = function()
    {

        sliderDrawer.layout.justifyContent = FlexLayout.JustifyContent.CENTER;
        sliderDrawer.layout.flexDirection =  FlexLayout.FlexDirection.COLUMN;
        sliderDrawer.layout.backgroundColor = Color.WHITE;

        var image1 = Image.createFromFile("images://avatar.png");
        var iconInfo    = Image.createFromFile("images://icon_info.png");
        var iconRequest     = Image.createFromFile("images://icon_request.png");
        var iconWorkList    = Image.createFromFile("images://icon_worklist.png");
        
        var myDataSet = [
                { 
                    title: "Status",//lang["pgRestaurants.title"], 
                    icon: iconInfo,
                    tag : PageConstants.PAGE_STATUS
                },
                { 
                    title: "New Request",//lang["pgMessages.title"], 
                    icon: iconRequest,
                    tag : PageConstants.PAGE_NEW_LEAVE_REQUEST
                },
                { 
                    title: "My Request",//lang["pgProfile.title"], 
                    icon: iconWorkList,
                    tag : PageConstants.PAGE_MY_LEAVE_REQUESTS
                }
                
            ]
       
       
        var topContainer = new FlexLayout();
        topContainer.flexGrow = 1.2;
        topContainer.flexBasis = 1;
        topContainer.justifyContent = FlexLayout.JustifyContent.CENTER;
        topContainer.alignItems     = FlexLayout.AlignItems.CENTER;
        topContainer.padding        = 10;
        topContainer.marginTop = 20;
        
        var profileImage = new ImageView();
        profileImage.image = Image.createFromFile("images://avatar.png");
        profileImage.maxHeight = 100;
        profileImage.imageFillType =  ImageView.FillType.ASPECTFIT;
        //profileImage.alignSelf = FlexLayout.AlignSelf.STRETCH;
        
        topContainer.addChild(profileImage);
        
        var dividerTop = new FlexLayout();
        dividerTop.height = 1;
        dividerTop.backgroundColor = Color.create('#20FFFFFF');
        
       
        var listView = new ListView({
            itemCount: myDataSet.length,
            rowHeight: 60,
            flexGrow : 2,
            backgroundColor : Color.TRANSPARENT,
            onRowCreate: function(){
                var rowImage = new ImageView({
                    id: 3,
                    width : 50,
                    imageFillType: ImageView.FillType.ASPECTFIT,
                    margin : 10,
                })
                
                var rowTitle = new Label();
                rowTitle.flexGrow = 1;
                rowTitle.flexBasis = 1;
                rowTitle.id = 4;
                //rowTitle.font =  Font.create("Lato",18,Font.NORMAL);
                rowTitle.backgroundColor = Color.TRANSPARENT;
                rowTitle.textColor = Color.BLACK;
                rowTitle.textAlignment = TextAlignment.MIDLEFT;
                rowTitle.alignSelf = FlexLayout.AlignSelf.CENTER;

                var rowTemplate = new ListViewItem({});
                rowTemplate.alignItems = FlexLayout.AlignItems.STRETCH;
                rowTemplate.flexDirection = FlexLayout.FlexDirection.ROW;
                rowTemplate.paddingLeft = 10;
                rowTemplate.paddingTop = 5;
                rowTemplate.paddingBottom = 5;
                
                rowTemplate.alpha = 0.5;
                rowTemplate.addChild(rowImage);
                rowTemplate.addChild(rowTitle);

                return rowTemplate;
            },
            onRowBind: function(listViewItem, index) {
                var rowTitle = listViewItem.findChildById(4);
                rowTitle.text = myDataSet[index].title;
                
                var rowImage = listViewItem.findChildById(3)
                rowImage.image = myDataSet[index].icon;
                
                if(Router.getCurrent() == myDataSet[index].tag)
                {
                    listViewItem.alpha = 1;
                }else
                {
                    listViewItem.alpha = 0.5;
                }

            },
            onRowSelected: function(listViewItem, index) {
                if(Router.getCurrent() != myDataSet[index].tag)
                {
                    
                    //global.Pages.currentTag = myDataSet[index].tag
                    //var nextpage = new (require(myDataSet[index].url));
                    //global.Pages.push(nextpage, false);
                    Router.go(myDataSet[index].tag,{},false);
                }
                listView.refreshData();
                
            },
            
            onPullRefresh: function(){
                //listView.itemCount = listView.itemCount + 10;
                //listView.refreshData();
                listView.stopRefresh();
            }
        });
        
        var dividerBottom = new FlexLayout();
        dividerBottom.height = 1;
        dividerBottom.backgroundColor = Color.create('#20FFFFFF');
        
        
        var btnSignOut = new Label();
        btnSignOut.height = 60;
        //btnSignOut.font =  Font.create("Lato",16,Font.NORMAL);
        btnSignOut.backgroundColor = Color.TRANSPARENT;
        btnSignOut.textColor = Color.BLACK;
        btnSignOut.textAlignment = TextAlignment.MIDLEFT;
        btnSignOut.text = "Sign Out"//lang['pgSliderDrawer.signout'];
        btnSignOut.alpha = 0.5;
        btnSignOut.marginLeft = 30;
        btnSignOut.touchEnabled = true;
        
        btnSignOut.onTouch = function ()
        {
            //var nextpage = new(require('pages/pgLogin'));
            //global.Pages.sliderDrawer.hide();
            //global.Pages.push(nextpage,false)
            Router.go(PageConstants.PAGE_LOGIN);
        }
                    
        sliderDrawer.layout.addChild(topContainer);      
        sliderDrawer.layout.addChild(dividerTop)     
        sliderDrawer.layout.addChild(listView);
        sliderDrawer.layout.addChild(dividerBottom);   
        sliderDrawer.layout.addChild(btnSignOut);
    }
    
   Router.sliderDrawer = sliderDrawer;
   Router.sliderDrawer.enabled = false;

   //Pages.sliderDrawer = sliderDrawer;
   //sliderDrawer.show();
}


