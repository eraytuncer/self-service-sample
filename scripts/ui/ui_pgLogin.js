/* 
		WARNING 
		Auto generated file. 
		Do not modify it's contents.
*/

const extend = require('js-base/core/extend');
const Page = require('nf-core/ui/page');

const FlexLayout = require('nf-core/ui/flexlayout');
const Color = require('nf-core/ui/color');
const ImageView = require('nf-core/ui/imageview');
const ImageFillType = require('nf-core/ui/imagefilltype');
const Image = require('nf-core/ui/image');
const Label = require('nf-core/ui/label');
const TextAlignment = require('nf-core/ui/textalignment');
const Font = require('nf-core/ui/font');
const TextBox = require('nf-core/ui/textbox');


const PgLogin_ = extend(Page)(
	//constructor
	function(_super){
		// initalizes super class for this page scope
		_super(this, {
			onLoad: onLoad.bind(this)
		});
		
		var rootLayout = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 80, 227, 194),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var topLayout = new FlexLayout({
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN_REVERSE,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var bottomLayout = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 235, 235, 235),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.CENTER,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1.5,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var textboxLayout = new FlexLayout({
			visible: true,
			width: 320,
			height: 125,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 0,
			marginRight: 0,
			marginBottom: 0,
			marginTop: 75
		});
		
	
		var imageview1 = new ImageView({
			imageFillType: ImageFillType.STRETCH,
			visible: true,
			top: 0,
			left: 0,
			image: Image.createFromFile("images://home_back.png"),
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			positionType: FlexLayout.PositionType.ABSOLUTE,
			right: 0,
			bottom: 0
		});
		
	
		var label1 = new Label({
			width: 250,
			height: 40,
			text: "EBS",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 30
		});
		label1.font = Font.create("Arial", 36, Font.NORMAL);
	
		var label2 = new Label({
			width: 250,
			height: 40,
			text: "Self Service",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 30,
			marginBottom: 10
		});
		label2.font = Font.create("Arial", 36, Font.NORMAL);
	
		var label3 = new Label({
			height: 25,
			text: "\"Powered & secured by Oracle MCS & ICS\"",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 30,
			marginBottom: 50
		});
		label3.font = Font.create("Arial", 14, Font.NORMAL);
	
		var usernameTextbox = new TextBox({
			height: 55,
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 204, 204, 204),
			borderWidth: 1,
			textColor: Color.create(255, 0, 0, 0),
			textAlignment: TextAlignment.MIDLEFT,
			borderRadius: 2.5,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		usernameTextbox.font = Font.create("Arial", 16, Font.NORMAL);
	
		var passwordTextbox = new TextBox({
			height: 55,
			visible: true,
			backgroundColor: Color.create(255, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 204, 204, 204),
			borderWidth: 1,
			textColor: Color.create(255, 0, 0, 0),
			textAlignment: TextAlignment.MIDLEFT,
			borderRadius: 2.5,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginTop: 10
		});
		passwordTextbox.font = Font.create("Arial", 16, Font.NORMAL);
	
		var loginLayout = new FlexLayout({
			height: 55,
			width: 320,
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.CENTER,
			justifyContent: FlexLayout.JustifyContent.CENTER,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var loginLabel = new Label({
			text: "Login",
			visible: true,
			width: 320,
			height: 55,
			backgroundColor: Color.create(255, 74, 144, 226),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.MIDCENTER,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		loginLabel.font = Font.create("Arial", 22, Font.NORMAL);
	
		var loadingImage = new ImageView({
			imageFillType: ImageFillType.ASPECTFIT,
			visible: true,
			top: 0,
			left: 0,
			image: Image.createFromFile("images://loading_0.png"),
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			positionType: FlexLayout.PositionType.ABSOLUTE,
			right: 0,
			bottom: 0
		});
		
	
		var label4 = new Label({
			width: 320,
			height: 25,
			text: "Please login with your MCS realm user.",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			alignSelf: FlexLayout.AlignSelf.AUTO
		});
		label4.font = Font.create("Arial", 12, Font.NORMAL);
	
		var children = {
			rootLayout: rootLayout
		};
		
		var orderedChildrenOfPage = [
			"rootLayout"
		];
		
		this.mapChildren = mapChildren.bind(this, orderedChildrenOfPage, children);
		
		var childrenOfrootLayout = {
			topLayout: topLayout,
			bottomLayout: bottomLayout
		};
		
		var orderedChildrenOfrootLayout = [
			"topLayout",
			"bottomLayout"
		];
		
		rootLayout.mapChildren = mapChildren.bind(rootLayout, orderedChildrenOfrootLayout, childrenOfrootLayout);
		
		var childrenOftopLayout = {
			imageview1: imageview1,
			label3: label3,
			label2: label2,
			label1: label1
		};
		
		var orderedChildrenOftopLayout = [
			"imageview1",
			"label3",
			"label2",
			"label1"
		];
		
		topLayout.mapChildren = mapChildren.bind(topLayout, orderedChildrenOftopLayout, childrenOftopLayout);
		
		var childrenOfbottomLayout = {
			textboxLayout: textboxLayout,
			loginLayout: loginLayout,
			label4: label4
		};
		
		var orderedChildrenOfbottomLayout = [
			"textboxLayout",
			"loginLayout",
			"label4"
		];
		
		bottomLayout.mapChildren = mapChildren.bind(bottomLayout, orderedChildrenOfbottomLayout, childrenOfbottomLayout);
		
		var childrenOftextboxLayout = {
			usernameTextbox: usernameTextbox,
			passwordTextbox: passwordTextbox
		};
		
		var orderedChildrenOftextboxLayout = [
			"usernameTextbox",
			"passwordTextbox"
		];
		
		textboxLayout.mapChildren = mapChildren.bind(textboxLayout, orderedChildrenOftextboxLayout, childrenOftextboxLayout);
		
		var childrenOfloginLayout = {
			loadingImage: loadingImage,
			loginLabel: loginLabel
		};
		
		var orderedChildrenOfloginLayout = [
			"loadingImage",
			"loginLabel"
		];
		
		loginLayout.mapChildren = mapChildren.bind(loginLayout, orderedChildrenOfloginLayout, childrenOfloginLayout);
		
});

const mapChildren = function(ordersOfchildren, children, callback){
	callback = callback.bind(this);
	ordersOfchildren.map(function(child){
		callback(children[child], child);
	});
};

function onLoad() { 
    this.headerBar.visible = false;
    this.headerBar.title = "newPage001";
    this.headerBar.titleColor = Color.create("#000000");
    this.headerBar.backgroundColor = Color.create("#FFFFFF");
    this.statusBar.visible = false;
    this.statusBar.android && (this.statusBar.android.color = Color.create("#00A1F1"));
    this.layout.alignContent = FlexLayout.AlignContent.STRETCH;
    this.layout.alignItems = FlexLayout.AlignItems.STRETCH;
    this.layout.direction = FlexLayout.Direction.INHERIT;
    this.layout.flexDirection = FlexLayout.FlexDirection.COLUMN;
    this.layout.flexWrap = FlexLayout.FlexWrap.NOWRAP;
    this.layout.justifyContent = FlexLayout.JustifyContent.FLEX_START;
    this.layout.backgroundColor = Color.create("#FFFFFF");
    //add components to page.
    this.mapChildren(function(component){
        if(component.mapChildren){
            addChild(component);
        }
        this.layout.addChild(component);
    });
}

//add child components to parent component.
function addChild(component){
    component.mapChildren(function(child){
        if(child.mapChildren){
            addChild(child);
        }
        this.addChild(child);
    });
}

module && (module.exports = PgLogin_);
