/* 
		WARNING 
		Auto generated file. 
		Do not modify it's contents.
*/

const extend = require('js-base/core/extend');
const Page = require('nf-core/ui/page');

const FlexLayout = require('nf-core/ui/flexlayout');
const Color = require('nf-core/ui/color');
const ImageView = require('nf-core/ui/imageview');
const ImageFillType = require('nf-core/ui/imagefilltype');
const Image = require('nf-core/ui/image');
const Label = require('nf-core/ui/label');
const TextAlignment = require('nf-core/ui/textalignment');
const Font = require('nf-core/ui/font');


const PgStatus_ = extend(Page)(
	//constructor
	function(_super){
		// initalizes super class for this page scope
		_super(this, {
			onLoad: onLoad.bind(this)
		});
		
		var rootLayout = new FlexLayout({
			visible: true,
			top: 1,
			left: 1,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.ABSOLUTE,
			bottom: 1,
			right: 1
		});
		
	
		var profileLayout = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(0, 126, 211, 33),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 0.34,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var progressLayout = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 65, 117, 5),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 0.09
		});
		
	
		var statisticsLayout = new FlexLayout({
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.FLEX_END,
			justifyContent: FlexLayout.JustifyContent.SPACE_AROUND,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 0.29,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var progressBar = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 234, 192, 211),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 0.75,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var remaningBar = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.WRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			right: 0,
			flexGrow: 0.25,
			bottom: 0
		});
		
	
		var statusLayout = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 231, 231, 231),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.CENTER,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 0.11
		});
		
	
		var bottomLayout = new FlexLayout({
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.CENTER,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 0.18,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var profileImageView = new ImageView({
			imageFillType: ImageFillType.STRETCH,
			visible: true,
			top: 1,
			left: 1,
			image: Image.createFromFile("images://home_back.png"),
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			positionType: FlexLayout.PositionType.ABSOLUTE,
			right: 1,
			bottom: 1
		});
		
	
		var flexlayout1 = new FlexLayout({
			visible: true,
			top: 1,
			left: 1,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.WRAP,
			flexGrow: 0,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.ABSOLUTE,
			right: 1,
			bottom: 1,
			alignSelf: FlexLayout.AlignSelf.AUTO
		});
		
	
		var nameLabel = new Label({
			text: "Lee Allen",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		nameLabel.font = Font.create("Arial", 24, Font.NORMAL);
	
		var titleLabel = new Label({
			text: "Director / HR Team",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1,
			marginTop: 5
		});
		titleLabel.font = Font.create("Arial", 16, Font.NORMAL);
	
		var avatar = new ImageView({
			imageFillType: ImageFillType.ASPECTFIT,
			visible: true,
			maxHeight: 100,
			image: Image.createFromFile("images://avatar.png"),
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 5,
			marginTop: 10
		});
		
	
		var totalLayout = new FlexLayout({
			visible: true,
			width: 100,
			height: 100,
			backgroundColor: Color.create(255, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 152, 152, 152),
			borderWidth: 1,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginBottom: 20
		});
		
	
		var flexlayout2 = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			alignSelf: FlexLayout.AlignSelf.STRETCH
		});
		
	
		var rightArrowImage = new ImageView({
			width: 30,
			height: 30,
			imageFillType: ImageFillType.ASPECTFIT,
			visible: true,
			image: Image.createFromFile("images://right_arrow.png"),
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			positionType: FlexLayout.PositionType.RELATIVE,
			alignSelf: FlexLayout.AlignSelf.AUTO,
			marginRight: 15
		});
		
	
		var statusTopLabel = new Label({
			text: "OUT OF OFFICE STATUS",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 203, 162, 181),
			textAlignment: TextAlignment.BOTTOMLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1,
			marginLeft: 15
		});
		statusTopLabel.font = Font.create("Arial", 16, Font.NORMAL);
	
		var flexlayout3 = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 15,
			marginTop: 5
		});
		
	
		var modeLabel = new Label({
			text: "Mode Off",
			visible: true,
			backgroundColor: Color.create(0, 238, 12, 12),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 0, 0, 0),
			textAlignment: TextAlignment.TOPLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 2,
			marginLeft: 10
		});
		modeLabel.font = Font.create("Arial", 16, Font.NORMAL);
	
		var label2 = new Label({
			visible: true,
			multiline: true,
			text: "You have 1 request(s) in total. The last one is on 12/22/2016",
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.MIDCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1,
			alignSelf: FlexLayout.AlignSelf.STRETCH,
			marginLeft: 15,
			marginRight: 15,
			marginTop: 15,
			marginBottom: 15
		});
		label2.font = Font.create("Arial", 12, Font.NORMAL);
	
		var plusImage = new ImageView({
			width: 75,
			height: 75,
			imageFillType: ImageFillType.ASPECTFIT,
			visible: true,
			image: Image.createFromFile("images://btn_plus.png"),
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginRight: 15
		});
		
	
		var totalNumberLabel = new Label({
			text: "37",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 152, 152, 152),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 2.5
		});
		totalNumberLabel.font = Font.create("Arial", 52, Font.BOLD);
	
		var label3 = new Label({
			text: "Total",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 152, 152, 152),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		label3.font = Font.create("Arial", 12, Font.NORMAL);
	
		var usedLayout = new FlexLayout({
			visible: true,
			width: 100,
			height: 100,
			backgroundColor: Color.create(0, 0, 0, 0),
			alpha: 1,
			borderColor: Color.create(255, 204, 163, 181),
			borderWidth: 1,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginBottom: 20
		});
		
	
		var totalnumberlabel_1 = new Label({
			text: "18",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 204, 163, 181),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 2.5
		});
		totalnumberlabel_1.font = Font.create("Arial", 52, Font.BOLD);
	
		var label3_1 = new Label({
			text: "Used",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 204, 163, 181),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		label3_1.font = Font.create("Arial", 12, Font.NORMAL);
	
		var remainingLayout = new FlexLayout({
			visible: true,
			width: 100,
			height: 100,
			backgroundColor: Color.create(255, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 152, 152, 152),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginBottom: 20
		});
		
	
		var totalnumberlabel_1_1 = new Label({
			text: "19",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 0, 0, 0),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 2.5
		});
		totalnumberlabel_1_1.font = Font.create("Arial", 52, Font.BOLD);
	
		var label3_1_1 = new Label({
			text: "Remaining",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 0, 0, 0),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		label3_1_1.font = Font.create("Arial", 12, Font.NORMAL);
	
		var seperatorLine = new FlexLayout({
			height: 3,
			visible: true,
			backgroundColor: Color.create(255, 169, 169, 169),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var imageview2 = new ImageView({
			imageFillType: ImageFillType.ASPECTFIT,
			visible: true,
			top: 1,
			left: 1,
			image: Image.createFromFile("images://square_stripe.png"),
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			positionType: FlexLayout.PositionType.ABSOLUTE,
			right: 1,
			bottom: 1
		});
		
	
		var label1 = new Label({
			width: 100,
			text: "Out Of Office",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 161, 161, 161),
			textAlignment: TextAlignment.TOPLEFT,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		label1.font = Font.create("Arial", 16, Font.NORMAL);
	
		var children = {
			rootLayout: rootLayout
		};
		
		var orderedChildrenOfPage = [
			"rootLayout"
		];
		
		this.mapChildren = mapChildren.bind(this, orderedChildrenOfPage, children);
		
		var childrenOfrootLayout = {
			profileLayout: profileLayout,
			progressLayout: progressLayout,
			statisticsLayout: statisticsLayout,
			seperatorLine: seperatorLine,
			statusLayout: statusLayout,
			bottomLayout: bottomLayout
		};
		
		var orderedChildrenOfrootLayout = [
			"profileLayout",
			"progressLayout",
			"statisticsLayout",
			"seperatorLine",
			"statusLayout",
			"bottomLayout"
		];
		
		rootLayout.mapChildren = mapChildren.bind(rootLayout, orderedChildrenOfrootLayout, childrenOfrootLayout);
		
		var childrenOfprofileLayout = {
			profileImageView: profileImageView,
			flexlayout1: flexlayout1
		};
		
		var orderedChildrenOfprofileLayout = [
			"profileImageView",
			"flexlayout1"
		];
		
		profileLayout.mapChildren = mapChildren.bind(profileLayout, orderedChildrenOfprofileLayout, childrenOfprofileLayout);
		
		var childrenOfprogressLayout = {
			progressBar: progressBar,
			remaningBar: remaningBar
		};
		
		var orderedChildrenOfprogressLayout = [
			"progressBar",
			"remaningBar"
		];
		
		progressLayout.mapChildren = mapChildren.bind(progressLayout, orderedChildrenOfprogressLayout, childrenOfprogressLayout);
		
		var childrenOfstatisticsLayout = {
			totalLayout: totalLayout,
			usedLayout: usedLayout,
			remainingLayout: remainingLayout
		};
		
		var orderedChildrenOfstatisticsLayout = [
			"totalLayout",
			"usedLayout",
			"remainingLayout"
		];
		
		statisticsLayout.mapChildren = mapChildren.bind(statisticsLayout, orderedChildrenOfstatisticsLayout, childrenOfstatisticsLayout);
		
		var childrenOfstatusLayout = {
			flexlayout2: flexlayout2,
			rightArrowImage: rightArrowImage
		};
		
		var orderedChildrenOfstatusLayout = [
			"flexlayout2",
			"rightArrowImage"
		];
		
		statusLayout.mapChildren = mapChildren.bind(statusLayout, orderedChildrenOfstatusLayout, childrenOfstatusLayout);
		
		var childrenOfbottomLayout = {
			label2: label2,
			plusImage: plusImage
		};
		
		var orderedChildrenOfbottomLayout = [
			"label2",
			"plusImage"
		];
		
		bottomLayout.mapChildren = mapChildren.bind(bottomLayout, orderedChildrenOfbottomLayout, childrenOfbottomLayout);
		
		var childrenOfflexlayout1 = {
			avatar: avatar,
			nameLabel: nameLabel,
			titleLabel: titleLabel
		};
		
		var orderedChildrenOfflexlayout1 = [
			"avatar",
			"nameLabel",
			"titleLabel"
		];
		
		flexlayout1.mapChildren = mapChildren.bind(flexlayout1, orderedChildrenOfflexlayout1, childrenOfflexlayout1);
		
		var childrenOftotalLayout = {
			totalNumberLabel: totalNumberLabel,
			label3: label3
		};
		
		var orderedChildrenOftotalLayout = [
			"totalNumberLabel",
			"label3"
		];
		
		totalLayout.mapChildren = mapChildren.bind(totalLayout, orderedChildrenOftotalLayout, childrenOftotalLayout);
		
		var childrenOfflexlayout2 = {
			statusTopLabel: statusTopLabel,
			flexlayout3: flexlayout3
		};
		
		var orderedChildrenOfflexlayout2 = [
			"statusTopLabel",
			"flexlayout3"
		];
		
		flexlayout2.mapChildren = mapChildren.bind(flexlayout2, orderedChildrenOfflexlayout2, childrenOfflexlayout2);
		
		var childrenOfflexlayout3 = {
			label1: label1,
			modeLabel: modeLabel
		};
		
		var orderedChildrenOfflexlayout3 = [
			"label1",
			"modeLabel"
		];
		
		flexlayout3.mapChildren = mapChildren.bind(flexlayout3, orderedChildrenOfflexlayout3, childrenOfflexlayout3);
		
		var childrenOfusedLayout = {
			totalnumberlabel_1: totalnumberlabel_1,
			label3_1: label3_1
		};
		
		var orderedChildrenOfusedLayout = [
			"totalnumberlabel_1",
			"label3_1"
		];
		
		usedLayout.mapChildren = mapChildren.bind(usedLayout, orderedChildrenOfusedLayout, childrenOfusedLayout);
		
		var childrenOfremainingLayout = {
			imageview2: imageview2,
			totalnumberlabel_1_1: totalnumberlabel_1_1,
			label3_1_1: label3_1_1
		};
		
		var orderedChildrenOfremainingLayout = [
			"imageview2",
			"totalnumberlabel_1_1",
			"label3_1_1"
		];
		
		remainingLayout.mapChildren = mapChildren.bind(remainingLayout, orderedChildrenOfremainingLayout, childrenOfremainingLayout);
		
});

const mapChildren = function(ordersOfchildren, children, callback){
	callback = callback.bind(this);
	ordersOfchildren.map(function(child){
		callback(children[child], child);
	});
};

function onLoad() { 
    this.headerBar.visible = true;
    this.headerBar.title = "Self Service";
    this.headerBar.titleColor = Color.create("#000000");
    this.headerBar.backgroundColor = Color.create("#FFFFFF");
    this.statusBar.visible = true;
    this.statusBar.android && (this.statusBar.android.color = Color.create("#00A1F1"));
    this.layout.backgroundColor = Color.create("#FFFFFF");
    this.layout.alignContent = FlexLayout.AlignContent.STRETCH;
    this.layout.alignItems = FlexLayout.AlignItems.STRETCH;
    this.layout.direction = FlexLayout.Direction.INHERIT;
    this.layout.flexDirection = FlexLayout.FlexDirection.COLUMN;
    this.layout.flexWrap = FlexLayout.FlexWrap.NOWRAP;
    this.layout.justifyContent = FlexLayout.JustifyContent.FLEX_START;
    //add components to page.
    this.mapChildren(function(component){
        if(component.mapChildren){
            addChild(component);
        }
        this.layout.addChild(component);
    });
}

//add child components to parent component.
function addChild(component){
    component.mapChildren(function(child){
        if(child.mapChildren){
            addChild(child);
        }
        this.addChild(child);
    });
}

module && (module.exports = PgStatus_);
