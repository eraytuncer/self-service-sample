/* 
		WARNING 
		Auto generated file. 
		Do not modify it's contents.
*/

const extend = require('js-base/core/extend');
const Page = require('nf-core/ui/page');

const FlexLayout = require('nf-core/ui/flexlayout');
const Color = require('nf-core/ui/color');
const Label = require('nf-core/ui/label');
const TextAlignment = require('nf-core/ui/textalignment');
const Font = require('nf-core/ui/font');
const ImageView = require('nf-core/ui/imageview');
const ImageFillType = require('nf-core/ui/imagefilltype');
const Image = require('nf-core/ui/image');


const PgMyRequest_ = extend(Page)(
	//constructor
	function(_super){
		// initalizes super class for this page scope
		_super(this, {
			onLoad: onLoad.bind(this)
		});
		
		var rootLayout = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 65, 117, 5),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutOne = new FlexLayout({
			visible: true,
			height: 130,
			backgroundColor: Color.create(255, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 255, 255, 255),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutTwo = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 65, 117, 5),
			alpha: 1,
			borderColor: Color.create(255, 255, 255, 255),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 0.14,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutThree = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 0.19,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutFour = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 232, 232, 232),
			alpha: 1,
			borderColor: Color.create(255, 255, 255, 255),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 0.53,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var removeButton = new Label({
			height: 70,
			visible: true,
			text: "< REMOVE >",
			backgroundColor: Color.create(255, 236, 43, 59),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.MIDCENTER,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		removeButton.font = Font.create("Arial", 28, Font.BOLD);
	
		var layoutTwoLeft = new FlexLayout({
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var label2 = new Label({
			height: 25,
			text: "LEAVE TYPE",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 44, 141, 250),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10,
			marginTop: 10
		});
		label2.font = Font.create("Arial", 16, Font.NORMAL);
	
		var label2_1 = new Label({
			height: 25,
			text: "PERSONAL",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10
		});
		label2_1.font = Font.create("Arial", 20, Font.NORMAL);
	
		var layoutTwoRight = new FlexLayout({
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var label2_2 = new Label({
			height: 25,
			text: "TIME UNIT",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 44, 141, 250),
			textAlignment: TextAlignment.MIDRIGHT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginTop: 10,
			marginRight: 20
		});
		label2_2.font = Font.create("Arial", 16, Font.NORMAL);
	
		var label2_1_1 = new Label({
			height: 25,
			text: "DAY",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.MIDRIGHT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginRight: 20
		});
		label2_1_1.font = Font.create("Arial", 20, Font.NORMAL);
	
		var layoutThreeLeft = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutThreeMiddle = new FlexLayout({
			visible: true,
			width: 75,
			backgroundColor: Color.create(255, 45, 141, 249),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutThreeRight = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var label3 = new Label({
			height: 25,
			text: "STARTS",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 44, 141, 250),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10,
			marginTop: 10
		});
		label3.font = Font.create("Arial", 16, Font.NORMAL);
	
		var label4 = new Label({
			height: 35,
			text: "12.22.16",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10
		});
		label4.font = Font.create("Arial", 30, Font.NORMAL);
	
		var label5 = new Label({
			height: 25,
			text: "ENDS",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 44, 141, 250),
			textAlignment: TextAlignment.MIDRIGHT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginRight: 10,
			marginTop: 10
		});
		label5.font = Font.create("Arial", 16, Font.NORMAL);
	
		var label5_1 = new Label({
			height: 35,
			text: "12.23.16",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.MIDRIGHT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginRight: 10
		});
		label5_1.font = Font.create("Arial", 30, Font.NORMAL);
	
		var label6 = new Label({
			text: "1",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1.5
		});
		label6.font = Font.create("Arial", 42, Font.NORMAL);
	
		var label7 = new Label({
			text: "days",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		label7.font = Font.create("Arial", 14, Font.NORMAL);
	
		var label8 = new Label({
			height: 25,
			text: "DESCRIPTION",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 44, 141, 250),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10,
			marginTop: 10
		});
		label8.font = Font.create("Arial", 16, Font.NORMAL);
	
		var line = new FlexLayout({
			height: 1,
			visible: true,
			backgroundColor: Color.create(255, 189, 189, 189),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var line_1 = new FlexLayout({
			height: 1,
			visible: true,
			backgroundColor: Color.create(255, 189, 189, 189),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var line_1_1 = new FlexLayout({
			height: 1,
			visible: true,
			backgroundColor: Color.create(255, 189, 189, 189),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutOneRight = new FlexLayout({
			visible: true,
			top: 0,
			left: 0,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.FLEX_START,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.ROW_REVERSE,
			positionType: FlexLayout.PositionType.ABSOLUTE,
			right: 0,
			bottom: 0
		});
		
	
		var rightBox = new FlexLayout({
			height: 60,
			width: 60,
			visible: true,
			backgroundColor: Color.create(0, 233, 23, 23),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginRight: 5,
			marginTop: 10
		});
		
	
		var middleBox = new FlexLayout({
			height: 60,
			width: 60,
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 204, 162, 181),
			borderWidth: 1,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginRight: 5,
			marginTop: 10
		});
		
	
		var leftBox = new FlexLayout({
			height: 60,
			width: 60,
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 163, 163, 163),
			borderWidth: 1,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginRight: 5,
			marginTop: 10
		});
		
	
		var imageview2 = new ImageView({
			imageFillType: ImageFillType.STRETCH,
			visible: true,
			top: 0,
			left: 0,
			image: Image.createFromFile("images://square_stripe.png"),
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			positionType: FlexLayout.PositionType.ABSOLUTE,
			right: 0,
			bottom: 0
		});
		
	
		var rightBoxNumber = new Label({
			text: "19",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		rightBoxNumber.font = Font.create("Arial", 32, Font.NORMAL);
	
		var label9_1 = new Label({
			text: "Rem.",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 0.5
		});
		label9_1.font = Font.create("Arial", 12, Font.NORMAL);
	
		var middleBoxNumber = new Label({
			text: "18",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 204, 162, 181),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		middleBoxNumber.font = Font.create("Arial", 32, Font.NORMAL);
	
		var label9_2 = new Label({
			text: "Used",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 204, 162, 181),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 0.5
		});
		label9_2.font = Font.create("Arial", 12, Font.NORMAL);
	
		var leftBoxNumber = new Label({
			text: "37",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 163, 163, 163),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		leftBoxNumber.font = Font.create("Arial", 32, Font.NORMAL);
	
		var label9_3 = new Label({
			text: "Total",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 163, 163, 163),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 0.5
		});
		label9_3.font = Font.create("Arial", 12, Font.NORMAL);
	
		var layoutOneLeft = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		
	
		var avatar = new ImageView({
			imageFillType: ImageFillType.ASPECTFIT,
			visible: true,
			width: 60,
			height: 60,
			image: Image.createFromFile("images://avatar.png"),
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			positionType: FlexLayout.PositionType.RELATIVE,
			alignSelf: FlexLayout.AlignSelf.FLEX_START,
			marginTop: 10,
			marginLeft: 10
		});
		
	
		var nameLabel = new Label({
			height: 35,
			text: "Lee Allen",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 48, 187, 105),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10,
			marginTop: 0
		});
		nameLabel.font = Font.create("Arial", 32, Font.NORMAL);
	
		var titleLabel = new Label({
			height: 20,
			text: "Director / HR Team",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 0, 0, 0),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10
		});
		titleLabel.font = Font.create("Arial", 16, Font.NORMAL);
	
		var label1 = new Label({
			visible: true,
			multiline: true,
			text: "I need a day off",
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.TOPLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 20,
			marginTop: 20,
			marginRight: 20,
			marginBottom: 20,
			flexGrow: 1
		});
		label1.font = Font.create("Arial", 16, Font.NORMAL);
	
		var children = {
			rootLayout: rootLayout
		};
		
		var orderedChildrenOfPage = [
			"rootLayout"
		];
		
		this.mapChildren = mapChildren.bind(this, orderedChildrenOfPage, children);
		
		var childrenOfrootLayout = {
			layoutOne: layoutOne,
			line: line,
			layoutTwo: layoutTwo,
			line_1: line_1,
			layoutThree: layoutThree,
			line_1_1: line_1_1,
			layoutFour: layoutFour,
			removeButton: removeButton
		};
		
		var orderedChildrenOfrootLayout = [
			"layoutOne",
			"line",
			"layoutTwo",
			"line_1",
			"layoutThree",
			"line_1_1",
			"layoutFour",
			"removeButton"
		];
		
		rootLayout.mapChildren = mapChildren.bind(rootLayout, orderedChildrenOfrootLayout, childrenOfrootLayout);
		
		var childrenOflayoutOne = {
			layoutOneLeft: layoutOneLeft,
			layoutOneRight: layoutOneRight
		};
		
		var orderedChildrenOflayoutOne = [
			"layoutOneLeft",
			"layoutOneRight"
		];
		
		layoutOne.mapChildren = mapChildren.bind(layoutOne, orderedChildrenOflayoutOne, childrenOflayoutOne);
		
		var childrenOflayoutTwo = {
			layoutTwoLeft: layoutTwoLeft,
			layoutTwoRight: layoutTwoRight
		};
		
		var orderedChildrenOflayoutTwo = [
			"layoutTwoLeft",
			"layoutTwoRight"
		];
		
		layoutTwo.mapChildren = mapChildren.bind(layoutTwo, orderedChildrenOflayoutTwo, childrenOflayoutTwo);
		
		var childrenOflayoutThree = {
			layoutThreeLeft: layoutThreeLeft,
			layoutThreeMiddle: layoutThreeMiddle,
			layoutThreeRight: layoutThreeRight
		};
		
		var orderedChildrenOflayoutThree = [
			"layoutThreeLeft",
			"layoutThreeMiddle",
			"layoutThreeRight"
		];
		
		layoutThree.mapChildren = mapChildren.bind(layoutThree, orderedChildrenOflayoutThree, childrenOflayoutThree);
		
		var childrenOflayoutFour = {
			label8: label8,
			label1: label1
		};
		
		var orderedChildrenOflayoutFour = [
			"label8",
			"label1"
		];
		
		layoutFour.mapChildren = mapChildren.bind(layoutFour, orderedChildrenOflayoutFour, childrenOflayoutFour);
		
		var childrenOflayoutTwoLeft = {
			label2: label2,
			label2_1: label2_1
		};
		
		var orderedChildrenOflayoutTwoLeft = [
			"label2",
			"label2_1"
		];
		
		layoutTwoLeft.mapChildren = mapChildren.bind(layoutTwoLeft, orderedChildrenOflayoutTwoLeft, childrenOflayoutTwoLeft);
		
		var childrenOflayoutTwoRight = {
			label2_2: label2_2,
			label2_1_1: label2_1_1
		};
		
		var orderedChildrenOflayoutTwoRight = [
			"label2_2",
			"label2_1_1"
		];
		
		layoutTwoRight.mapChildren = mapChildren.bind(layoutTwoRight, orderedChildrenOflayoutTwoRight, childrenOflayoutTwoRight);
		
		var childrenOflayoutThreeLeft = {
			label3: label3,
			label4: label4
		};
		
		var orderedChildrenOflayoutThreeLeft = [
			"label3",
			"label4"
		];
		
		layoutThreeLeft.mapChildren = mapChildren.bind(layoutThreeLeft, orderedChildrenOflayoutThreeLeft, childrenOflayoutThreeLeft);
		
		var childrenOflayoutThreeMiddle = {
			label6: label6,
			label7: label7
		};
		
		var orderedChildrenOflayoutThreeMiddle = [
			"label6",
			"label7"
		];
		
		layoutThreeMiddle.mapChildren = mapChildren.bind(layoutThreeMiddle, orderedChildrenOflayoutThreeMiddle, childrenOflayoutThreeMiddle);
		
		var childrenOflayoutThreeRight = {
			label5: label5,
			label5_1: label5_1
		};
		
		var orderedChildrenOflayoutThreeRight = [
			"label5",
			"label5_1"
		];
		
		layoutThreeRight.mapChildren = mapChildren.bind(layoutThreeRight, orderedChildrenOflayoutThreeRight, childrenOflayoutThreeRight);
		
		var childrenOflayoutOneRight = {
			rightBox: rightBox,
			middleBox: middleBox,
			leftBox: leftBox
		};
		
		var orderedChildrenOflayoutOneRight = [
			"rightBox",
			"middleBox",
			"leftBox"
		];
		
		layoutOneRight.mapChildren = mapChildren.bind(layoutOneRight, orderedChildrenOflayoutOneRight, childrenOflayoutOneRight);
		
		var childrenOfrightBox = {
			imageview2: imageview2,
			rightBoxNumber: rightBoxNumber,
			label9_1: label9_1
		};
		
		var orderedChildrenOfrightBox = [
			"imageview2",
			"rightBoxNumber",
			"label9_1"
		];
		
		rightBox.mapChildren = mapChildren.bind(rightBox, orderedChildrenOfrightBox, childrenOfrightBox);
		
		var childrenOfmiddleBox = {
			middleBoxNumber: middleBoxNumber,
			label9_2: label9_2
		};
		
		var orderedChildrenOfmiddleBox = [
			"middleBoxNumber",
			"label9_2"
		];
		
		middleBox.mapChildren = mapChildren.bind(middleBox, orderedChildrenOfmiddleBox, childrenOfmiddleBox);
		
		var childrenOfleftBox = {
			leftBoxNumber: leftBoxNumber,
			label9_3: label9_3
		};
		
		var orderedChildrenOfleftBox = [
			"leftBoxNumber",
			"label9_3"
		];
		
		leftBox.mapChildren = mapChildren.bind(leftBox, orderedChildrenOfleftBox, childrenOfleftBox);
		
		var childrenOflayoutOneLeft = {
			avatar: avatar,
			nameLabel: nameLabel,
			titleLabel: titleLabel
		};
		
		var orderedChildrenOflayoutOneLeft = [
			"avatar",
			"nameLabel",
			"titleLabel"
		];
		
		layoutOneLeft.mapChildren = mapChildren.bind(layoutOneLeft, orderedChildrenOflayoutOneLeft, childrenOflayoutOneLeft);
		
});

const mapChildren = function(ordersOfchildren, children, callback){
	callback = callback.bind(this);
	ordersOfchildren.map(function(child){
		callback(children[child], child);
	});
};

function onLoad() { 
    this.headerBar.visible = true;
    this.headerBar.title = "My Leave Request";
    this.headerBar.titleColor = Color.create(255, 44, 141, 250);
    this.headerBar.backgroundColor = Color.create("#FFFFFF");
    this.statusBar.visible = true;
    this.statusBar.android && (this.statusBar.android.color = Color.create("#00A1F1"));
    this.layout.alignContent = FlexLayout.AlignContent.STRETCH;
    this.layout.alignItems = FlexLayout.AlignItems.STRETCH;
    this.layout.direction = FlexLayout.Direction.INHERIT;
    this.layout.flexDirection = FlexLayout.FlexDirection.COLUMN;
    this.layout.flexWrap = FlexLayout.FlexWrap.NOWRAP;
    this.layout.justifyContent = FlexLayout.JustifyContent.FLEX_START;
    this.layout.backgroundColor = Color.create("#FFFFFF");
    //add components to page.
    this.mapChildren(function(component){
        if(component.mapChildren){
            addChild(component);
        }
        this.layout.addChild(component);
    });
}

//add child components to parent component.
function addChild(component){
    component.mapChildren(function(child){
        if(child.mapChildren){
            addChild(child);
        }
        this.addChild(child);
    });
}

module && (module.exports = PgMyRequest_);
