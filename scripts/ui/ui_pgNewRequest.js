/* 
		WARNING 
		Auto generated file. 
		Do not modify it's contents.
*/

const extend = require('js-base/core/extend');
const Page = require('nf-core/ui/page');

const FlexLayout = require('nf-core/ui/flexlayout');
const Color = require('nf-core/ui/color');
const Label = require('nf-core/ui/label');
const TextAlignment = require('nf-core/ui/textalignment');
const Font = require('nf-core/ui/font');
const ImageView = require('nf-core/ui/imageview');
const ImageFillType = require('nf-core/ui/imagefilltype');
const Image = require('nf-core/ui/image');
const TextBox = require('nf-core/ui/textbox');


const PgNewRequest_ = extend(Page)(
	//constructor
	function(_super){
		// initalizes super class for this page scope
		_super(this, {
			onLoad: onLoad.bind(this)
		});
		
		var rootLayout = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 65, 117, 5),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutOne = new FlexLayout({
			visible: true,
			height: 75,
			backgroundColor: Color.create(255, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 255, 255, 255),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.CENTER,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutTwo = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 65, 117, 5),
			alpha: 1,
			borderColor: Color.create(255, 255, 255, 255),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 0.14,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutThree = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 0.19,
			flexDirection: FlexLayout.FlexDirection.ROW,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutFour = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(255, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 255, 255, 255),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 0.53,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var approveButton = new Label({
			height: 70,
			visible: true,
			text: "√",
			backgroundColor: Color.create(255, 128, 210, 53),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.MIDCENTER,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		approveButton.font = Font.create("Verdana", 32, Font.BOLD);
	
		var leftBox = new FlexLayout({
			height: 60,
			width: 60,
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 163, 163, 163),
			borderWidth: 1,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10
		});
		
	
		var leftboxNumber = new Label({
			text: "37",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 152, 152, 152),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 2
		});
		leftboxNumber.font = Font.create("Arial", 32, Font.NORMAL);
	
		var label1 = new Label({
			text: "Total",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 152, 152, 152),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		label1.font = Font.create("Arial", 12, Font.NORMAL);
	
		var middleBox = new FlexLayout({
			height: 60,
			width: 60,
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 204, 162, 181),
			borderWidth: 1,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10
		});
		
	
		var middleboxNumber = new Label({
			text: "18",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 204, 162, 181),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 2
		});
		middleboxNumber.font = Font.create("Arial", 32, Font.NORMAL);
	
		var label1_1 = new Label({
			text: "Used",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 204, 162, 181),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		label1_1.font = Font.create("Arial", 12, Font.NORMAL);
	
		var rightBox = new FlexLayout({
			height: 60,
			width: 60,
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(0, 255, 255, 255),
			borderWidth: 1,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10
		});
		
	
		var rightboxNumber = new Label({
			text: "19",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 2
		});
		rightboxNumber.font = Font.create("Arial", 32, Font.NORMAL);
	
		var label1_1_1 = new Label({
			text: "Rem.",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		label1_1_1.font = Font.create("Arial", 12, Font.NORMAL);
	
		var imageview1 = new ImageView({
			imageFillType: ImageFillType.STRETCH,
			visible: true,
			top: 1,
			left: 1,
			image: Image.createFromFile("images://square_stripe.png"),
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			positionType: FlexLayout.PositionType.ABSOLUTE,
			right: 1,
			bottom: 1
		});
		
	
		var layoutTwoLeft = new FlexLayout({
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var label2 = new Label({
			height: 25,
			text: "LEAVE TYPE ▼",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 44, 141, 250),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10,
			marginTop: 10
		});
		label2.font = Font.create("Arial", 16, Font.NORMAL);
	
		var label2_1 = new Label({
			height: 25,
			text: "PERSONAL",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10
		});
		label2_1.font = Font.create("Arial", 20, Font.NORMAL);
	
		var layoutTwoRight = new FlexLayout({
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var label2_2 = new Label({
			height: 25,
			text: "TIME UNIT ▼",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 44, 141, 250),
			textAlignment: TextAlignment.MIDRIGHT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginTop: 10,
			marginRight: 20
		});
		label2_2.font = Font.create("Arial", 16, Font.NORMAL);
	
		var label2_1_1 = new Label({
			height: 25,
			text: "DAY",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.MIDRIGHT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginRight: 20
		});
		label2_1_1.font = Font.create("Arial", 20, Font.NORMAL);
	
		var layoutThreeLeft = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutThreeMiddle = new FlexLayout({
			visible: true,
			width: 75,
			backgroundColor: Color.create(255, 45, 141, 249),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var layoutThreeRight = new FlexLayout({
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexGrow: 1,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var label3 = new Label({
			height: 25,
			text: "STARTS ▼",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 44, 141, 250),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10,
			marginTop: 10
		});
		label3.font = Font.create("Arial", 16, Font.NORMAL);
	
		var label4 = new Label({
			height: 35,
			text: "03.15.17",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10
		});
		label4.font = Font.create("Arial", 30, Font.NORMAL);
	
		var label5 = new Label({
			height: 25,
			text: "ENDS ▼",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 44, 141, 250),
			textAlignment: TextAlignment.MIDRIGHT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginRight: 10,
			marginTop: 10
		});
		label5.font = Font.create("Arial", 16, Font.NORMAL);
	
		var label5_1 = new Label({
			height: 35,
			text: "03.22.17",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create("#000000"),
			textAlignment: TextAlignment.MIDRIGHT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginRight: 10
		});
		label5_1.font = Font.create("Arial", 30, Font.NORMAL);
	
		var label6 = new Label({
			text: "7",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.BOTTOMCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1.5
		});
		label6.font = Font.create("Arial", 42, Font.NORMAL);
	
		var label7 = new Label({
			text: "days",
			visible: true,
			backgroundColor: Color.create(0, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 255, 255, 255),
			textAlignment: TextAlignment.TOPCENTER,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1
		});
		label7.font = Font.create("Arial", 14, Font.NORMAL);
	
		var label8 = new Label({
			height: 25,
			text: "DESCRIPTION",
			visible: true,
			backgroundColor: Color.create(255, 255, 255, 255),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 44, 141, 250),
			textAlignment: TextAlignment.MIDLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			marginLeft: 10,
			marginTop: 10
		});
		label8.font = Font.create("Arial", 16, Font.NORMAL);
	
		var textboxDescription = new TextBox({
			text: "Please add your \"Absence\" reason briefly",
			visible: true,
			backgroundColor: Color.create("#FFFFFF"),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			textColor: Color.create(255, 174, 174, 174),
			textAlignment: TextAlignment.TOPLEFT,
			positionType: FlexLayout.PositionType.RELATIVE,
			flexGrow: 1,
			marginLeft: 20,
			marginTop: 20,
			marginBottom: 20,
			marginRight: 20
		});
		textboxDescription.font = Font.create("Arial", 16, Font.NORMAL);
	
		var line = new FlexLayout({
			height: 3,
			visible: true,
			backgroundColor: Color.create(255, 189, 189, 189),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var line_1 = new FlexLayout({
			height: 1,
			visible: true,
			backgroundColor: Color.create(255, 189, 189, 189),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var line_1_1 = new FlexLayout({
			height: 1,
			visible: true,
			backgroundColor: Color.create(255, 189, 189, 189),
			alpha: 1,
			borderColor: Color.create(255, 0, 0, 0),
			borderWidth: 0,
			alignContent: FlexLayout.AlignContent.STRETCH,
			alignItems: FlexLayout.AlignItems.STRETCH,
			justifyContent: FlexLayout.JustifyContent.FLEX_START,
			flexWrap: FlexLayout.FlexWrap.NOWRAP,
			flexDirection: FlexLayout.FlexDirection.COLUMN,
			positionType: FlexLayout.PositionType.RELATIVE
		});
		
	
		var children = {
			rootLayout: rootLayout
		};
		
		var orderedChildrenOfPage = [
			"rootLayout"
		];
		
		this.mapChildren = mapChildren.bind(this, orderedChildrenOfPage, children);
		
		var childrenOfrootLayout = {
			layoutOne: layoutOne,
			line: line,
			layoutTwo: layoutTwo,
			line_1: line_1,
			layoutThree: layoutThree,
			line_1_1: line_1_1,
			layoutFour: layoutFour,
			approveButton: approveButton
		};
		
		var orderedChildrenOfrootLayout = [
			"layoutOne",
			"line",
			"layoutTwo",
			"line_1",
			"layoutThree",
			"line_1_1",
			"layoutFour",
			"approveButton"
		];
		
		rootLayout.mapChildren = mapChildren.bind(rootLayout, orderedChildrenOfrootLayout, childrenOfrootLayout);
		
		var childrenOflayoutOne = {
			leftBox: leftBox,
			middleBox: middleBox,
			rightBox: rightBox
		};
		
		var orderedChildrenOflayoutOne = [
			"leftBox",
			"middleBox",
			"rightBox"
		];
		
		layoutOne.mapChildren = mapChildren.bind(layoutOne, orderedChildrenOflayoutOne, childrenOflayoutOne);
		
		var childrenOflayoutTwo = {
			layoutTwoLeft: layoutTwoLeft,
			layoutTwoRight: layoutTwoRight
		};
		
		var orderedChildrenOflayoutTwo = [
			"layoutTwoLeft",
			"layoutTwoRight"
		];
		
		layoutTwo.mapChildren = mapChildren.bind(layoutTwo, orderedChildrenOflayoutTwo, childrenOflayoutTwo);
		
		var childrenOflayoutThree = {
			layoutThreeLeft: layoutThreeLeft,
			layoutThreeMiddle: layoutThreeMiddle,
			layoutThreeRight: layoutThreeRight
		};
		
		var orderedChildrenOflayoutThree = [
			"layoutThreeLeft",
			"layoutThreeMiddle",
			"layoutThreeRight"
		];
		
		layoutThree.mapChildren = mapChildren.bind(layoutThree, orderedChildrenOflayoutThree, childrenOflayoutThree);
		
		var childrenOflayoutFour = {
			label8: label8,
			textboxDescription: textboxDescription
		};
		
		var orderedChildrenOflayoutFour = [
			"label8",
			"textboxDescription"
		];
		
		layoutFour.mapChildren = mapChildren.bind(layoutFour, orderedChildrenOflayoutFour, childrenOflayoutFour);
		
		var childrenOfleftBox = {
			leftboxNumber: leftboxNumber,
			label1: label1
		};
		
		var orderedChildrenOfleftBox = [
			"leftboxNumber",
			"label1"
		];
		
		leftBox.mapChildren = mapChildren.bind(leftBox, orderedChildrenOfleftBox, childrenOfleftBox);
		
		var childrenOfmiddleBox = {
			middleboxNumber: middleboxNumber,
			label1_1: label1_1
		};
		
		var orderedChildrenOfmiddleBox = [
			"middleboxNumber",
			"label1_1"
		];
		
		middleBox.mapChildren = mapChildren.bind(middleBox, orderedChildrenOfmiddleBox, childrenOfmiddleBox);
		
		var childrenOfrightBox = {
			imageview1: imageview1,
			rightboxNumber: rightboxNumber,
			label1_1_1: label1_1_1
		};
		
		var orderedChildrenOfrightBox = [
			"imageview1",
			"rightboxNumber",
			"label1_1_1"
		];
		
		rightBox.mapChildren = mapChildren.bind(rightBox, orderedChildrenOfrightBox, childrenOfrightBox);
		
		var childrenOflayoutTwoLeft = {
			label2: label2,
			label2_1: label2_1
		};
		
		var orderedChildrenOflayoutTwoLeft = [
			"label2",
			"label2_1"
		];
		
		layoutTwoLeft.mapChildren = mapChildren.bind(layoutTwoLeft, orderedChildrenOflayoutTwoLeft, childrenOflayoutTwoLeft);
		
		var childrenOflayoutTwoRight = {
			label2_2: label2_2,
			label2_1_1: label2_1_1
		};
		
		var orderedChildrenOflayoutTwoRight = [
			"label2_2",
			"label2_1_1"
		];
		
		layoutTwoRight.mapChildren = mapChildren.bind(layoutTwoRight, orderedChildrenOflayoutTwoRight, childrenOflayoutTwoRight);
		
		var childrenOflayoutThreeLeft = {
			label3: label3,
			label4: label4
		};
		
		var orderedChildrenOflayoutThreeLeft = [
			"label3",
			"label4"
		];
		
		layoutThreeLeft.mapChildren = mapChildren.bind(layoutThreeLeft, orderedChildrenOflayoutThreeLeft, childrenOflayoutThreeLeft);
		
		var childrenOflayoutThreeMiddle = {
			label6: label6,
			label7: label7
		};
		
		var orderedChildrenOflayoutThreeMiddle = [
			"label6",
			"label7"
		];
		
		layoutThreeMiddle.mapChildren = mapChildren.bind(layoutThreeMiddle, orderedChildrenOflayoutThreeMiddle, childrenOflayoutThreeMiddle);
		
		var childrenOflayoutThreeRight = {
			label5: label5,
			label5_1: label5_1
		};
		
		var orderedChildrenOflayoutThreeRight = [
			"label5",
			"label5_1"
		];
		
		layoutThreeRight.mapChildren = mapChildren.bind(layoutThreeRight, orderedChildrenOflayoutThreeRight, childrenOflayoutThreeRight);
		
});

const mapChildren = function(ordersOfchildren, children, callback){
	callback = callback.bind(this);
	ordersOfchildren.map(function(child){
		callback(children[child], child);
	});
};

function onLoad() { 
    this.headerBar.visible = true;
    this.headerBar.title = "New Leave Request";
    this.headerBar.titleColor = Color.create(255, 44, 141, 250);
    this.headerBar.backgroundColor = Color.create("#FFFFFF");
    this.statusBar.visible = true;
    this.statusBar.android && (this.statusBar.android.color = Color.create("#00A1F1"));
    this.layout.alignContent = FlexLayout.AlignContent.STRETCH;
    this.layout.alignItems = FlexLayout.AlignItems.STRETCH;
    this.layout.direction = FlexLayout.Direction.INHERIT;
    this.layout.flexDirection = FlexLayout.FlexDirection.COLUMN;
    this.layout.flexWrap = FlexLayout.FlexWrap.NOWRAP;
    this.layout.justifyContent = FlexLayout.JustifyContent.FLEX_START;
    this.layout.backgroundColor = Color.create("#FFFFFF");
    //add components to page.
    this.mapChildren(function(component){
        if(component.mapChildren){
            addChild(component);
        }
        this.layout.addChild(component);
    });
}

//add child components to parent component.
function addChild(component){
    component.mapChildren(function(child){
        if(child.mapChildren){
            addChild(child);
        }
        this.addChild(child);
    });
}

module && (module.exports = PgNewRequest_);
